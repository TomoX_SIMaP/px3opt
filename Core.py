import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d

from px3opt.Material import _GenericMaterial
from px3opt.Source import _GenericSource
from px3opt.Functions import *
from px3opt.Results import Results
from px3opt.Detector import Detector

class Session:
    '''
    Master class that holds all the optimization: problem definition, solver, results and visualization

    :param mata,matb: Materials of the sample between wich contrast to noise ratio as to be otpimized
    :type mata,matb: :py:mod:`Material`
    :param  src_list: List of source setups
    :type src_list: list of :py:mod:`Source`
    :param detector: Detector instance that hold parameters of the detector
    :type detector: :py:mod:`Detector`
    :param array Lth_range,Hth_range: 1D array that defines the evaluation points for the low and high threshold respectively
    :param int or None FL: Desired count level for flat images.  If None, input flux is defined by sources in src_list. Otherwise, for each threshold configurations, input spectrum is scaled by a multipicative factor so that the most exposed register counts FL.

    :ivar mata:
    :ivar matb:
    :ivar  src_list:
    :ivar detector:
    :ivar Lth_range:
    :ivar Hth_range:
    :ivar FL:
    :ivar results: Object that holds all results from optimization
    :vartype results: :py:mod:`Results`
    '''
    def __init__(self,mata, matb,  src_list, detector, Lth_range, Hth_range, FL=None):

        if not issubclass(type(mata), _GenericMaterial):
            raise TypeError('mata should be a Material object')

        if not issubclass(type(matb), _GenericMaterial):
            raise TypeError('matb should be a Material object')

        if not all(issubclass(type(src), _GenericSource) for src in src_list):
            raise TypeError('src_list should contain only Source objects')

        if not isinstance(detector, Detector):
            raise TypeError('detector should eb a Detector object')

        if (FL is not None) and (not np.isscalar(FL)):
            raise TypeError('FL should be a scalar or None')

        for src in src_list:
            if (mata.Erange.min() > src.Erange.min() or mata.Erange.max() < src.Erange.max()
                    or matb.Erange.min() > src.Erange.min() or matb.Erange.max() < src.Erange.max() ):
                raise ValueError("all source definition Erange should be included in materials definition Erange")

        self.detector = detector

        # Make spectra of material regular then smooth depending ons spectral_res of detector
        self.mata = mata
        if not isregular(mata.Erange):
            self.mata.Erange = np.linspace(mata.Erange[0], mata.Erange[-1],mata.Erange.size)
            self.mata.A = np.interp(self.mata.Erange, mata.Erange, mata.A)
        self.mata.A = gaussian_filter1d(self.mata.A,
                                        self.detector.spectral_res / (self.mata.Erange[1] - self.mata.Erange[0]))

        self.matb = matb
        if not isregular(matb.Erange):
            self.matb.Erange = np.linspace(matb.Erange[0], matb.Erange[-1],matb.Erange.size)
            self.matb.A = np.interp(self.matb.Erange, matb.Erange, matb.A)
        self.matb.A = gaussian_filter1d(self.matb.A,
                                        self.detector.spectral_res / (self.matb.Erange[1] - self.matb.Erange[0]))

        self.src_list = src_list

        self.Lth_range = Lth_range
        self.Hth_range = Hth_range
        self.FL = FL

        self.results = Results(self)

    def compute(self):
        '''
        Execute the optimization
        '''
        for s, src in enumerate(self.src_list):
            I0 = src.I0 * self.detector.px_size ** 2 * 1e-8  # per cm2 -> per pixel
            A_a = np.interp(src.Erange, self.mata.Erange, self.mata.A)
            A_b = np.interp(src.Erange, self.matb.Erange, self.matb.A)

            for l, L_th in enumerate(self.Lth_range):
                for h, H_th in enumerate(self.Hth_range):

                    if H_th <= L_th:
                        self._set_result_points([s,l,h], [np.nan]*9)
                    else:
                        self._set_result_points([s,l,h], CN_ln(src.Erange,A_a, A_b, I0, L_th, H_th,self.FL))

        self.results.maps.arePresent = True
        self.results.findOptima()

    def _set_result_points(self, coord, res):
        coord = tuple(coord)
        self.results.maps.I_0_1[coord] = res[0]
        self.results.maps.I_0_2[coord] = res[1]
        self.results.maps.sigma_ABS[coord] = res[2]
        self.results.maps.sigma_KES[coord] = res[3]
        self.results.maps.C_ABS[coord] = res[4]
        self.results.maps.C_KES[coord] = res[5]
        self.results.maps.CNR_ABS[coord] = res[6]
        self.results.maps.CNR_KES[coord] = res[7]
        self.results.maps.mult[coord] = res[8]

        CNRs = [res[6], res[7]]
        id = np.argmax(CNRs)
        self.results.maps.bestCNR[coord] = CNRs[id]
        self.results.maps.modality[coord] = id

    def showSpectra(self, xlim=None, ylim=None):
        '''
        Display absorbance spectra of the two materials compared.

        :param [list] xlim: 2 elements iterable that contains limits of of energy range (keV) to be displayed.
        :param [list] xlim: 2 elements iterable that contains limits of the Absorbance range to be displayed.
        '''

        fig = plt.figure()
        ax = fig.add_subplot(111)
        if xlim is not None: ax.set_xlim(*xlim)
        if ylim is not None: ax.set_ylim(*ylim)
        ax.set_xlabel('Energy (keV)')
        ax.set_ylabel('Absorbance (-)')
        ax.plot(self.mata.Erange, self.mata.A, label=self.mata.label)
        ax.plot(self.matb.Erange, self.matb.A, label=self.matb.label)

        plt.legend()
        plt.show()

    def showMaps(self, quantities=['CNR_KES'], cmap='viridis'):
        '''
        Display maps of resulting quantities as function of thresholds

        :param quantities:  A list of quantities names beyond 'CNR_KES', 'CNR_ABS', 'C_KES', 'C_ABS', 'sigma_KES', 'sigma_ABS', 'I_0_1', 'I_0_2', 'mult' and 'bestCNR'
        :type quantities: list of str
        :param cmap: colormap used for the dipslay
        :type cmap: str

        :raise RuntimeError: if called before :py:meth:`~Core.Session.compute`
        '''
        accepetedQuantity = ['CNR_KES', 'CNR_ABS', 'C_KES', 'C_ABS', 'sigma_KES', 'sigma_ABS', 'I_0_1', 'I_0_2', 'mult', 'bestCNR']
        if not self.results.maps.arePresent:
            raise RuntimeError('need to run optimization before displaying any result')

        for Q in quantities:
            if Q not in accepetedQuantity:
                raise ValueError('quanities element should be belong:' + ', '.join(accepetedQuantity))

        X, Y = np.meshgrid(self.Lth_range - 0.5*(self.Lth_range[1] - self.Lth_range[0]),
                           self.Hth_range - 0.5*(self.Hth_range[1] - self.Hth_range[0]))

        ncol = len(self.src_list)
        nrow = len(quantities)

        fig = plt.figure(figsize=(4*ncol, 4*nrow))
        for q, Q in enumerate(quantities):
            resmap = getattr(self.results.maps, Q)

            for s, src in enumerate(self.src_list):
                ax = fig.add_subplot(nrow,ncol,q*ncol + s +1)
                ax.set_aspect('equal')
                if s == 0:
                    ax.set_ylabel('High threshold (keV)')
                if q == len(quantities)-1:
                    ax.set_xlabel('Low threshold (keV)')
                if q == 0:
                    ax.title.set_text(src.label)

                pc = ax.pcolormesh(X, Y, np.transpose(resmap[s]), cmap=cmap)
                if s == self.results.optima.KES_optimum_mapCoord[0]:
                    ax.plot(self.results.optima.KES_optimum[1], self.results.optima.KES_optimum[2], 'g.', label='KES optimum')
                if s == self.results.optima.ABS_optimum_mapCoord[0]:
                    ax.plot(self.results.optima.ABS_optimum[1], self.results.optima.ABS_optimum[2], 'r.',
                            label='ABS optimum')

                cb = fig.colorbar(pc, ax=ax)
                if s == len(self.src_list)-1:
                    cb.set_label(Q)

                if q*ncol + s+1 == ncol*nrow:
                    ax.legend(loc='lower right')

        plt.show()

if __name__ =='__main__':
    from px3opt.Material import MaterialByElement
    from px3opt.Source import SpeckPySource
    from px3opt.Detector import Detector

    mata = MaterialByElement('I', density=0.23, thickness=1)
    matb = MaterialByElement('Ba', density=0.24, thickness=1)
    PixiIII = Detector()

    src_list = [SpeckPySource(50),  SpeckPySource(60)]
    S = Session(mata,matb,src_list,
                  detector = PixiIII,
                  Lth_range=np.arange(20,50),
                  Hth_range=np.arange(20,50))

    S.compute()

    S.showMaps(quantities=['CNR_KES'])