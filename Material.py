'''
Module that holds Material object used in setup of the optimization.
Materials are represented by their absorbance spectra in the optimization.
'''
import numpy as np
import xraylib as xrl
import matplotlib.pyplot as plt
import re

h = 3.74491e-15  # eV s
c = 2.99792458e8  # m/s

class _GenericMaterial:
    '''
    Generic class, that holds the necessary attributes and method to comply with the :py:class:`Core.Session` class.
    '''
    def __init__(self):
        self.label = None
        self.Erange = None
        self.A = None

    def show(self, xlim=None):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.set_xlabel('Energy (keV)')
        ax.set_ylabel('Absorbance (-)')
        ax.title.set_text(self.label)
        ax.plot(self.Erange, self.A)
        if xlim is not None: ax.set_xlim(*xlim)
        plt.show()

    def __add__(self, other):
        '''
        Creates a material that represent the superposition of the two material added by summing their respective spectra, interpolating if needed.
        The label of the resulting material is set from the labels of the two added material.

        :param other: Another material object to be added with
        :type other: :py:mod:`material`
        :return: The resulting material
        :rtype: :py:mod:`material`
        '''
        CM = _GenericMaterial()
        CM.label = self.label + '+' + other.label
        if np.array_equal(self.Erange, other.Erange):
            CM.Erange = self.Erange
            CM.A = self.A + other.A
        else:
            minE = np.max([self.Erange[0], other.Erange[0]])
            maxE = np.min([self.Erange[-1], other.Erange[-1]])
            Erange = np.unique(np.concatenate((self.Erange, other.Erange)))
            Erange = Erange[Erange >= minE]
            Erange = Erange[Erange <= maxE]

            selfA_interp = np.interp(Erange, self.Erange, self.A)
            otherA_interp = np.interp(Erange, other.Erange, other.A)
            CM.Erange = Erange
            CM.A = selfA_interp + otherA_interp
        return CM

class MaterialByElement(_GenericMaterial):
    '''
    Creates a Material object given a chemical elements, its density and thickness of material

    :param str symbol: Chemical element symbol
    :param float density: in g/cm^3
    :param float thickness: in mm
    :param array or None Erange: 1D array that defines the energy bins of the absorbance spectrum, in keV. If None [1,160] will be taken.
    :param str or None label: a label to identify the material. If None, label will be set as <Symbol>_rho_<density>_thick_<thickness>.
    '''
    def __init__(self, symbol, thickness, density=None, Erange=None, label=None):
        super().__init__()
        self.symbol = symbol
        if density is None: density=xrl.ElementDensity(xrl.SymbolToAtomicNumber(symbol))
        self.density = density
        self.thickness = thickness
        if Erange is None: Erange = np.arange(1,160)
        if label is None:
            self.label = '%s_rho_%.2f_thick_%.2e'%(symbol, density, thickness)
        else:
            self.label = label
        self.Erange = Erange

        Im_n = np.empty_like(Erange, dtype=float)
        for e, E in enumerate(Erange):
            Im_n[e] = xrl.Refractive_Index_Im(symbol, float(E), density)
        self.A = (4 * np.pi * Erange * 1e3 * thickness * 1e-3) / (h * c) * Im_n

class MaterialFromSpectrum(_GenericMaterial):
    '''
    Creates a material given the absorbance spectra

    :param array Erange: 1d array of the energy bins of the spectrum
    :param array A: Same shape as Erange, give the absobance in each energy bin
    :param str label: A label to identify the material
    '''
    def __init__(self, Erange, A, label):
        super().__init__()
        self.Erange = Erange
        self.A = A
        self.label = label

class MaterialFromMassicComposition(_GenericMaterial):
    '''
    Create a Material made of several chemical elements given their massic percentages

    :param list Elements: List of of symbols (str) of the chemical elements
    :param array-like percentages: List of corresponding percentages
    :param float thickness: thickness of material, in mm
    :param float density: total density of the material, n g/cm^3
    :param array-like Erange: 1D array that defines the energy bins of the absorbance spectrum, in keV. If None [1,160] will be taken.
    :param str label: A label to identify the Material
    '''
    def __init__(self, Elements,percentages, thickness, density, Erange=None, label=None):
        if len(Elements) != len(percentages)+1:
            raise ValueError('len of Elements should be length of percentages + 1')

        Comp_Mat = MaterialByElement(Elements[0], thickness, (1-sum(percentages))*density, Erange)
        for i, prct in enumerate(percentages):
            Comp_Mat += MaterialByElement(Elements[i+1], thickness, prct*density, Erange)
        if Erange is None: Erange=np.arange(1,160)
        if label is None : label = ''.join([Elements[0]]+['%s%d'%(Elements[i+1], 100*percentages[i]) for i in range(len(percentages))])

        self.Elements = Elements
        self.percentages = percentages
        self.thickness = thickness
        self.density = density
        self.Erange  = Erange
        self.A = Comp_Mat.A
        self.label=label

class MaterialFromChemicalFormula(_GenericMaterial):
    '''
    Creates a material given its chemical formula and density.

    :param str molecule: The chemical formula of the molecule, e.g. 'H2O'
    :param float thickness: Thickness of material, in mm
    :param float density: Total density of the material, n g/cm^3
    :param array-like Erange: 1D array that defines the energy bins of the absorbance spectrum, in keV. If None [1,160] will be taken.
    :param str label: A label to identify the Material
    '''
    def __init__(self, molecule,  thickness,density, Erange=None, label=None):
        if type(molecule) != str:
            raise TypeError('molecule should be a string')
        if Erange is None: Erange = np.arange(1,160)
        if label is None: label = '%s_rho_%.2f_thick_%.2e'%(molecule, density, thickness)

        split = re.findall(r'[A-Z][a-z]*|\d+', re.sub('[A-Z][a-z]*(?![\da-z])', r'\g<0>1', molecule))
        Elements = split[::2]
        coeffs = np.array(split[1::2]).astype(int)
        AtomicWeights = []

        for e,El in enumerate(Elements):
            AtomicWeights.append(float(xrl.AtomicWeight(xrl.SymbolToAtomicNumber(El))))
        Molecular_weight = (np.array(AtomicWeights, dtype=float) * np.array(coeffs)).sum()

        Mat = MaterialByElement(Elements[0], thickness, coeffs[0]*AtomicWeights[0]/Molecular_weight*density, Erange)
        if len(Elements) >1:

            for e,El in enumerate(Elements[1:]):
                e += 1
                Mat += MaterialByElement(El,thickness,coeffs[e]*AtomicWeights[e]/Molecular_weight*density,Erange)

        self.molecule = molecule
        self.density = density
        self.thickness = thickness
        self.Erange = Erange
        self.A = Mat.A
        self.label = label

if __name__ == '__main__':
    import re
    cpd = 'KI'
    test = MaterialFromChemicalFormula(cpd, 1, 0.3)
    test .show()
    import matplotlib.pyplot as plt

    mata = 'I'
    matb = 'Ba'

    Erange = np.arange(5,50)
    thickness = 1

    Samp_A = MaterialByElement(mata, thickness, Erange=np.arange(10,45))
    Samp_B = MaterialByElement(matb, thickness, Erange=np.arange(5,30))
    Samp_A.show()
    CM = Samp_A+Samp_B

    fig= plt.figure()
    plt.plot(Samp_A.Erange, Samp_A.A, label=Samp_A.label)
    plt.plot(Samp_B.Erange, Samp_B.A, label=Samp_B.label)
    plt.plot(CM.Erange, CM.A, label=CM.label)
    plt.legend()
    plt.show()