'''
Module that holds the core models and computations.
'''
import numpy as np

def A_model(G, O, A, Gc):
    '''
    Parametrization of the amplitude in the exponential model for the noise.

    .. math::

        A\_model = O + Ae^{\\frac{-G}{Gc}}

    '''
    return O + A * np.exp(-G / Gc)

def Ic_model(G, A, C, Gl, F):
    '''
    Parametrization of the characteristic mean count level in the exponential model for the noise.
    '''
    return np.piecewise(G, [G < Gl, G >= Gl], [lambda x: C * x + A, lambda x: F])

def exp_model(I, A, Ic):
    '''
    Parametrization of the prefactor

    :param I: Mean count level
    :param A: Amplitude
    :param Ic: characteristic mean count level
    :return: Prefactor

    .. math::

        Prefactor = A \\times (1-e^{\\frac{-I}{Ic}})
    '''
    return A * (1 - np.exp(-I / Ic))

def model(I, Egap):
    '''
    Parametrization of the noise model for the low energy counter

    :param I: Input mean count level
    :type I: scalar
    :param Egap: Energy width between low and high threshold
    :type Egap: scalar
    :return: estimated standard deviation
    '''
    pA = [37.52160992, 19.95545852, 2.25234043]
    pIc = [191.6775183, 49.40973142, 10.00000002, 605.4243454]
    A = A_model(Egap, *pA)
    Ic = Ic_model(Egap, *pIc)
    Pref = exp_model(I, A, Ic)

    std = np.sqrt(I) * Pref
    return std

def model_H(I):
    '''
    Parametrization of the noise model for the High energy counter

    :param I:  Input mean count level
    :type I: scalar
    :return: estimated standard deviation
    '''

    pref = exp_model(I, 33, 295)

    std = np.sqrt(I) * pref
    return std

def _sigma_L(F, Egap):
    '''
    wrapping function
    '''

    return model(F, Egap)

def _sigma_H(F):
    '''
    wrapping function
    '''
    return model_H(F)

def CN_ln(Erange, A_a, A_b, I0, L_th, H_th, FL=None):
    '''
    Core function that evaluate flat levels, contrast, noise and CNR at given (L_th, H_th) config point.

    :param Erange: List of energies at which Absorbance and Input flux spectra are evaluated
    :param A_a: Absorbance spectrum of material A
    :param A_b: Absorbance spectrum of material B
    :param I0:  Input flux spectrum
    :param L_th: Low threshold (keV)
    :param H_th:  High threshold (keV)
    :param FL: If none, I0 is used unchanged, otherwise it's tune so that flat levels on register reahch FL
    :return:  I_0_1, I_0_2, sigma_ABS, sigma_KES, C_ABS, C_KES, CNR_ABS, CNR_KES, mult
    '''
    maskl = Erange >= L_th
    mask2 = Erange >= H_th
    mask1 = maskl * ~mask2

    # Compute Intensity spectra
    I_0_1 = I0[mask1].sum()
    I_0_2 = I0[mask2].sum()
    if FL is None:
        mult = 1
    else:
        mult = FL / max(I_0_1, I_0_2)
    I_0_1 *= mult
    I_0_2 *= mult
    I0 *= mult
    I_a = I0 * np.exp(-A_a)
    I_b = I0 * np.exp(-A_b)

    # Compute avg and sigma on raw data
    I_a_1 = I_a[mask1].sum()
    I_a_2 = I_a[mask2].sum()
    I_b_1 = I_b[mask1].sum()
    I_b_2 = I_b[mask2].sum()

    sigma_a_1 = _sigma_L(I_a_1, H_th - L_th)
    sigma_b_1 = _sigma_L(I_b_1, H_th - L_th)
    sigma_a_2 = _sigma_H(I_a_2)
    sigma_b_2 = _sigma_H(I_b_2)

    # Compute avg and sigma on processed data
    ## a_1
    AVG_FF_a_1 = I_a_1 / I_0_1
    VAR_FF_a_1 = sigma_a_1 ** 2 / I_0_1 ** 2

    AVG_ln_a_1 = np.log(AVG_FF_a_1) - 0.5 / (AVG_FF_a_1 ** 2) * VAR_FF_a_1
    VAR_ln_a_1 = VAR_FF_a_1 / AVG_FF_a_1 ** 2 - 0.25 * VAR_FF_a_1 ** 2 / AVG_FF_a_1 ** 4

    ## a_2
    AVG_FF_a_2 = I_a_2 / I_0_2
    VAR_FF_a_2 = sigma_a_2 ** 2 / I_0_2 ** 2

    AVG_ln_a_2 = np.log(AVG_FF_a_2) - 0.5 / (AVG_FF_a_2 ** 2) * VAR_FF_a_2
    VAR_ln_a_2 = VAR_FF_a_2 / AVG_FF_a_2 ** 2 - 0.25 * VAR_FF_a_2 ** 2 / AVG_FF_a_2 ** 4

    ## b_1
    AVG_FF_b_1 = I_b_1 / I_0_1
    VAR_FF_b_1 = sigma_b_1 ** 2 / I_0_1 ** 2

    AVG_ln_b_1 = np.log(AVG_FF_b_1) - 0.5 / (AVG_FF_b_1 ** 2) * VAR_FF_b_1
    VAR_ln_b_1 = VAR_FF_b_1 / AVG_FF_b_1 ** 2 - 0.25 * VAR_FF_b_1 ** 2 / AVG_FF_b_1 ** 4

    ## b_2
    AVG_FF_b_2 = I_b_2 / I_0_2
    VAR_FF_b_2 = sigma_b_2 ** 2 / I_0_2 ** 2

    AVG_ln_b_2 = np.log(AVG_FF_b_2) - 0.5 / (AVG_FF_b_2 ** 2) * VAR_FF_b_2
    VAR_ln_b_2 = VAR_FF_b_2 / AVG_FF_b_2 ** 2 - 0.25 * VAR_FF_b_2 ** 2 / AVG_FF_b_2 ** 4

    ## KES_a
    AVG_KES_a = AVG_ln_a_2 - AVG_ln_a_1
    VAR_KES_a = VAR_ln_a_2 + VAR_ln_a_1

    ## KES_b
    AVG_KES_b = AVG_ln_b_2 - AVG_ln_b_1
    VAR_KES_b = VAR_ln_b_2 + VAR_ln_b_1

    C_KES = np.abs(AVG_KES_a - AVG_KES_b)
    sigma_KES = 0.5 * (np.sqrt(VAR_KES_a) + np.sqrt(VAR_KES_b))
    CNR_KES = C_KES / sigma_KES

    # --- ABS
    AVG_ABS_a = (I_a_1 + I_a_2) / (I_0_1 + I_0_2)
    VAR_ABS_a = (sigma_a_1 ** 2 + sigma_a_2 ** 2) / (I_0_1 + I_0_2) ** 2

    AVG_ABS_b = (I_b_1 + I_b_2) / (I_0_1 + I_0_2)
    VAR_ABS_b = (sigma_b_1 ** 2 + sigma_b_2 ** 2) / (I_0_1 + I_0_2) ** 2

    C_ABS = np.abs(AVG_ABS_a - AVG_ABS_b)
    sigma_ABS = 0.5 * (np.sqrt(VAR_ABS_a) + np.sqrt(VAR_ABS_b))
    CNR_ABS = C_ABS / sigma_ABS

    return  I_0_1, I_0_2, sigma_ABS, sigma_KES, C_ABS, C_KES, CNR_ABS, CNR_KES, mult

def isregular(arr):
    steps = arr[1:] - arr[:-1]
    return np.unique(steps).size == 1
