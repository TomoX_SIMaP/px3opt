Code description
================

.. toctree::

    The central object: the Session class in the px3opt.Core module <./Core.rst>
    Source objects: the px3opt.Source module <./Source.rst>
    Sample objects: the px3opt.Material module <./Material.rst>
    Detector object: the px3opt.Detector module <./Detector.rst>
    The result structure: the px3opt.Results moodule <./Results.rst>
    Core computations: the px3opt.Functions module <./Functions.rst>