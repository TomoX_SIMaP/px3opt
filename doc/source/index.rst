.. px3opt documentation master file, created by
   sphinx-quickstart on Tue Apr 12 14:54:54 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================
px3opt documentation
====================
px3opt, standing for PixieIII OPTimization, is an optimization tool for acquisition with PixieIII Xray detector written in python.
It is based on modeling of the sample contrast and empirical parametrization of the detector behavior.

gitlab repository : https://gricad-gitlab.univ-grenoble-alpes.fr/TomoX_SIMaP/px3opt

An article is in preparation that described this developments and evaluates the model.

.. figure:: schema.png
   :scale: 50%

   Scheme of the the code structure and  workflow of the model

Contents
========

.. toctree::
   :maxdepth: 4

   self
   ./Requirements_installation.rst
   ./Getting_started.rst
   ./Content.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`